package ar.com.epabo;


import java.util.List;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.PlatformTransactionManager;

import ar.com.epabo.domain.EpaboDomainConfiguration;
import ar.com.epabo.domain.Wallet;
import ar.com.epabo.mapper.WalletMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { EpaboDomainConfiguration.class })
@EnableAutoConfiguration
public class EpaboApplicationTests {
	
	
	@Autowired
	@Qualifier("epaboDS")
	private DataSource epaboDS;

	@Autowired
	@Qualifier("epaboTxManager")
	PlatformTransactionManager transactionManager;

	@Autowired
	SqlSessionFactory sqlSessionFactory;

	@Test
	public void contextLoads() {
		SqlSession ss = sqlSessionFactory.openSession();
		WalletMapper mapper = ss.getMapper(WalletMapper.class);
		List<Wallet> list =mapper.getWalletsDomain();
		ss.close();
	}

}
