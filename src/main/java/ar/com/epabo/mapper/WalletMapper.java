package ar.com.epabo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import ar.com.epabo.domain.Wallet;

/*
 * private String userId;
	private String userName;
	private String userSurename;
	private String walletBalance;
 * 
 */

public interface WalletMapper {

	@Select("select user_id, user_name, user_surename, balance from wallets;")
	@Results({
		@Result(property="userId",column="user_id"),
		@Result(property="userName",column="user_name"),
		@Result(property="userSurename",column="user_surename"),
		@Result(property="walletBalance",column="balance")
	})
	public List<Wallet> getWalletsDomain();

}
