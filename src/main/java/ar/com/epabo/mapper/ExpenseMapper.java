/**
 * 
 */
package ar.com.epabo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import ar.com.epabo.domain.Expense;

/**
 * @author agustin
 *
 */
public interface ExpenseMapper {
	
	@Select("SELECT wallet_id, username, details, amount FROM expenses where wallet_id = #{id};")
	@Results({
		@Result(property="walletId", column="wallet_id"),
		@Result(property="userName", column="username"),
		@Result(property="details", column="details"),
		@Result(property="amount", column="amount")
	})
	public List<Expense> getExpensesById(int id);
	
	@Insert("INSERT INTO expenses (wallet_id, username, details, amount) "
			+ "VALUES (#{walletId}, #{userName}, #{details}, #{amount});")
	public void createExpense(Expense dto);
}
