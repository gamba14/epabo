package ar.com.epabo.controller;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ar.com.epabo.domain.Expense;
import ar.com.epabo.mapper.ExpenseMapper;

@RestController
public class ExpensesController {
	@Autowired
	SqlSessionFactory sqlSessionFactory;

	@RequestMapping("/v1.0/api/wallets/{id}")
	@ResponseBody
	public List<Expense> getWalletExpenses(@PathVariable("id") int id) {
		SqlSession ss = sqlSessionFactory.openSession();
		ExpenseMapper mapper = ss.getMapper(ExpenseMapper.class);
		List<Expense> expenses = mapper.getExpensesById(id);
		ss.close();
		return expenses;

	}

	@PostMapping("/v1.0/api/wallets/{id}/new")
	@ResponseBody
	public Expense createNewExpense(@PathVariable("id") int id, @RequestBody Expense expense) {
		SqlSession ss = sqlSessionFactory.openSession();
		ExpenseMapper mapper = ss.getMapper(ExpenseMapper.class);
		mapper.createExpense(expense);
		return expense;

	}

}
