package ar.com.epabo.controller;

import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.epabo.Greeter;

@RestController
public class GreeterControler {
	private static final Logger logs = LoggerFactory.getLogger(GreeterControler.class);
	private static String dir1;
	@Value("${epabo.rest.dir1}")	

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@RequestMapping("/v1.0/api/login")
	public Greeter greeter(@RequestParam(value = "name", defaultValue = "Hola") String name) {
		logs.debug("GET REQUEST");
		return new Greeter(counter.incrementAndGet(), String.format(template, name));
	}
	
	@RequestMapping("/v1.0/api/greeter")
	public String details() {
		return "Testing" + dir1;
	}

}
