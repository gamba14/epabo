/**
 * 
 */
package ar.com.epabo.wrappers;

import java.util.ArrayList;
import java.util.List;

import ar.com.epabo.domain.Expense;

/**
 * @author agustin
 *
 */
public class ExpenseList {
	private List<Expense> expenses;
	
	public void ExpensesList() {
		setExpenses(new ArrayList<>());
	}

	/**
	 * @return the expenses
	 */
	public List<Expense> getExpenses() {
		return expenses;
	}

	/**
	 * @param expenses the expenses to set
	 */
	public void setExpenses(List<Expense> expenses) {
		this.expenses = expenses;
	}
	

}
