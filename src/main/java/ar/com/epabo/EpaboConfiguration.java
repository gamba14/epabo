package ar.com.epabo;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "classpath:application.properties")
public class EpaboConfiguration {

	/*
	 * 
	 * CLASE ANOTADA PARA GENERAR CONFIGURACIONES
	 * 
	 * 
	 */

}
