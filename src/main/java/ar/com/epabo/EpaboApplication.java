	package ar.com.epabo;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class EpaboApplication {
	final static Logger log = LoggerFactory.getLogger(EpaboApplication.class);
	public static void main(String[] args) {
		BasicConfigurator.configure();
		SpringApplication.run(EpaboApplication.class, args);
		log.debug("Iniciando EPABO core con argumentos"+args);
		
	}
}
