package ar.com.epabo.domain;

public class Wallet {
	
	private String userId;
	private String userName;
	private String userSurename;
	private String walletBalance;
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the userSurename
	 */
	public String getUserSurename() {
		return userSurename;
	}
	/**
	 * @param userSurename the userSurename to set
	 */
	public void setUserSurename(String userSurename) {
		this.userSurename = userSurename;
	}
	/**
	 * @return the walletBalance
	 */
	public String getWalletBalance() {
		return walletBalance;
	}
	/**
	 * @param walletBalance the walletBalance to set
	 */
	public void setWalletBalance(String walletBalance) {
		this.walletBalance = walletBalance;
	}
	
	

}
