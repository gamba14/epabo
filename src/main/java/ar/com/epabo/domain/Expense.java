package ar.com.epabo.domain;

/*
 * DB SCHEMA
 * wallet_id int,	
  username VARCHAR(45) NOT NULL ,
  details VARCHAR(45) NOT NULL ,
  amount double NOT NULL ,
 * 
 * 
 * 
 */

public class Expense {
	
	private int walletId;
	private String userName;
	private String details;
	private double amount;
	/**
	 * @return the walletId
	 */
	public int getWalletId() {
		return walletId;
	}
	/**
	 * @param walletId the walletId to set
	 */
	public void setWalletId(int walletId) {
		this.walletId = walletId;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}
	/**
	 * @param details the details to set
	 */
	public void setDetails(String details) {
		this.details = details;
	}
	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	

}
