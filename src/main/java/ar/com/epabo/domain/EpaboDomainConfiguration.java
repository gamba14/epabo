/**
 * 
 */
package ar.com.epabo.domain;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import ar.com.epabo.mapper.WalletMapper;

/**
 * @author agustin
 *
 */
@Configuration
@ComponentScan(basePackageClasses = { EpaboDomainConfiguration.class })
@PropertySource(value = "classpath:epabo-domain.properties")
@MapperScan(basePackageClasses = WalletMapper.class)
public class EpaboDomainConfiguration {
	
	@Bean
	@ConfigurationProperties(prefix = "epabo.datasource")
	public HikariConfig hikariPoolConf() {
		return new HikariConfig();
	}

	@Bean(name = "epaboDS")
	public DataSource epaboDS(@Qualifier("hikariPoolConf") HikariConfig hc) {
		hc.addDataSourceProperty("characterEncoding","utf8");
		hc.addDataSourceProperty("useUnicode","true");
		return new HikariDataSource(hc);
	}

	@Bean(name = "epaboTxManager")
	public DataSourceTransactionManager transactionManager(@Qualifier("epaboDS") DataSource dataSource) {
		DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(dataSource);
		return transactionManager;
	}

	@Bean
	@ConfigurationProperties(prefix = "epabo.mybatis.configuration")
	public org.apache.ibatis.session.Configuration epaboMBConfig() {
		return new org.apache.ibatis.session.Configuration();
	}

	@Bean
	public SqlSessionFactory sqlSessionFactory(@Qualifier("epaboDS") DataSource dataSource,
			@Qualifier("epaboMBConfig") org.apache.ibatis.session.Configuration mbConfig) throws Exception {
		SqlSessionFactoryBean ss = new SqlSessionFactoryBean();
		ss.setDataSource(dataSource);
		ss.setConfiguration(mbConfig);
		return ss.getObject();

	}

}
